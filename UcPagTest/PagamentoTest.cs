using Microsoft.VisualStudio.TestTools.UnitTesting;
using UcPag;

namespace UcPagTest
{
    [TestClass]
    public class PagamentoTest
    {
        //M�todo para verificar se o valor retornado do m�todo gerartaxajuro
        [TestMethod]
        public void GerarTaxaJuros_VerificarRetorno()
        {
            //Organizando
            Pagamento Pag = new Pagamento();
            var esperado = 0.01;
            //A��o
            var resultado = Pag.GerarTaxaJuro();

            //Corrigindo
            Assert.AreEqual(esperado, resultado);
        }
        //M�todo para verificar se o valor retornado do calculo do juros � v�lido
        [TestMethod]
        public void CalcularJuros_ComValoresValidos()
        {
            //Organizando
            Pagamento Pag = new Pagamento();
            var periodo = 5;
            var valorInicial = 100.00;
            var ValorEsperado = 105.10;

            //A��o
            var resultado = Pag.CalcularJuros(valorInicial, periodo);

            //Corrigindo
            Assert.AreEqual(ValorEsperado, resultado,0.00,"Calculo efetuado com sucesso");
        }
        //M�todo para verificar se o valor retornado do calculo de juros � v�lido, utilizando valores inv�lidos
        [TestMethod]
        public void CalcularJuros_ComValorInicialInvalido()
        {
            //Organizando
            Pagamento Pag = new Pagamento();
            var periodo = 5;
            var ValorInicial = -100;
            //A��o
            //Corrigindo
            Assert.ThrowsException<System.ArgumentOutOfRangeException>(() => Pag.CalcularJuros(ValorInicial, periodo));
        }
        //M�todo de teste para verificar utilizando valor do periodo invalido
        [TestMethod]
        public void CalcularJuros_ComPeriodoInvalido()
        {
            //Organizando
            Pagamento Pag = new Pagamento();
            var periodo = -5;
            var ValorInicial = 100;
            //A��o
            //Corrigindo
            Assert.ThrowsException<System.ArgumentOutOfRangeException>(() => Pag.CalcularJuros(ValorInicial, periodo));
        }
        [TestMethod]
        public void CalcularJuros_SemValorInicial()
        {
            //Organizando
            Pagamento Pag = new Pagamento();
            var periodo = 5;
            var valorInicial = 0;
            //A��o
            //Corrigindo
            Assert.ThrowsException<System.ArgumentOutOfRangeException>(() => Pag.CalcularJuros(valorInicial, periodo));
        }
    }
}
