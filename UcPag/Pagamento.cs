﻿using System;

namespace UcPag
{
    public class Pagamento
    {
        //Atributos da classe
        private double taxa;
        private double juro;
        private int tempo;
        private double valorInicial;       

        //Métodos de acesso
        public double Taxa { get => taxa; set => taxa = value; }
        public double Juro { get => juro; set => juro = value; }
        public int Tempo { get => tempo; set => tempo = value; }
        public double ValorInicial { get => valorInicial; set => valorInicial = value; }
        //Métodos contrutores
        public Pagamento(double taxa, double juro, int tempo, double valorInicial)
        {
            this.taxa = taxa;
            this.juro = juro;
            this.tempo = tempo;
            this.valorInicial = valorInicial;
        }
        //Método construtor vazio
        public Pagamento ()
        { }
        //Método para Gerar Taxa de Juros
        public double GerarTaxaJuro()
        {
            return 0.01;            
        }
        //Método para calcular o Juros
        public double CalcularJuros(double valorinicial, int periodo)
        {
            //Verifica se os parametros são menores ou iguais a 0
            if(valorinicial <= 0 || periodo <= 0)
            {
                throw new ArgumentOutOfRangeException("Valores menores que zero não são válidos");
            }
            //elevando o resultado de valorinicial * (1 + 0,001) pelo periodo
            //Retornando valor, não arredondado. Somente com DUAS casas decimais
            var resultado = (Math.Pow(1 + GerarTaxaJuro(), periodo) * valorinicial, 2).ToString();
            var a = resultado.IndexOf(',',0,resultado.Length-1);            
            return Convert.ToDouble(resultado.Substring(1, a+2));
        }
        public static void Main()
        { }
    }
}
